import sqlite3 as sql
import simplejson
import json
from json import JSONEncoder
from json import JSONDecoder
from CrossDomain import crossdomain
#from datetime import timedelta
from flask import Flask, render_template, request, current_app, make_response, jsonify
from functools import update_wrapper
from datetime import date, datetime

myDB = "database.db"

app = Flask(__name__)

@app.route('/login', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def login():

    userList = []
    u_name = request.json['username']
    u_passwd = request.json['password']
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute('SELECT * FROM UserAccounts WHERE username=? AND password=?', (u_name, u_passwd))
        rows = cur.fetchall()
        for row in rows:
            user = {
                'id':row['id'],
                'f_name':row['f_name'],
                'l_name':row['l_name'],
                'username':row['username'],
                'password':row['password'],
                'email':row['email'],
                'mobile':row['mobile'],
                'userType':row['userType'],
                'defaultLocation':row['defaultLocation'],
                'prof_img':row['prof_img'],
                'f_id':row['f_id']
            }
            userList.append(user)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()

    if(len(userList) == 0):
        return "Failed"
    elif(len(userList) > 1):
        return "Failed"
    else:
        return jsonify(userList)


'''
    User Account Section
'''
#Return all users except the requesting user
@app.route('/getAllUserAccounts', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllUsers():
    
    userList = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM UserAccounts WHERE id!=? AND f_id=?",
                    (int(request.json['id']), int(request.json['f_id'])))
        rows = cur.fetchall()
        for row in rows:
            user = {
                'id':row['id'],
                'f_name':row['f_name'],
                'l_name':row['l_name'],
                'username':row['username'],
                'password':row['password'],
                'prof_img':row['prof_img'],
                'email':row['email'],
                'mobile':row['mobile'],
                'userType':row['userType'],
                'defaultLocation':row['defaultLocation']
            }
            userList.append(user)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(userList) == 0):
        return "None"
    
    return jsonify(userList)

#Returns the base 64 string for a user profile
@app.route('/getUserAccountImg', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getUserAccountImg():
    
    userList = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT id, prof_img FROM UserAccounts WHERE id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            user = {
                'id':row['id'],
                'prof_img':row['prof_img']
            }
            userList.append(user)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(userList) > 1):
        return "Multiple Users"
    
    return jsonify(userList)

#Add User
@app.route('/addNewUserAccount', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewUserAccount():
    
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        
        cur.execute("INSERT INTO UserAccounts (f_name, l_name, prof_img, username, password, email, \
                        mobile, userType, defaultLocation, f_id) \
                        VALUES (?,?,?,?,?,?,?,?,?,?)", (request.json['f_name'],
                                                    request.json['l_name'],
                                                    request.json['prof_img'],
                                                    request.json['username'],
                                                    request.json['password'],
                                                    request.json['email'],
                                                    request.json['mobile'],
                                                    request.json['userType'],
                                                    int(request.json['defaultLocation']),
                                                    int(request.json['f_id'])))
    
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        result = cur.lastrowid
        print(result)
        cur.close()
        con.close()
    

    return jsonify(result)

#Update User
@app.route('/updateUserAccount', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateUserAccount():
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE UserAccounts SET f_name=?, l_name=?, prof_img=?, username=?, \
                    password=?, email=?, mobile=?, userType=?, \
                    defaultLocation=? WHERE id=?", (request.json['f_name'],
                                                    request.json['l_name'],
                                                    request.json['prof_img'],
                                                    request.json['username'],
                                                    request.json['password'],
                                                    request.json['email'],
                                                    request.json['mobile'],
                                                    request.json['userType'],
                                                    request.json['defaultLocation'],
                                                    int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete User
@app.route('/deleteUserAccount', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteUserAccount():
    
    result = -1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from UserAccounts WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        if(con.total_changes > 0):
            result = 1
        else:
            result = 0
        cur.close()
        con.close()

    return jsonify(result)




'''
    Location Section
'''
#List all locations for a given franchise
@app.route('/getAllLocations', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllLocations():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Locations WHERE f_id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'name':row['name'],
                'address':row['address'],
                'num_screens':row['num_screens'],
                'l_status':row['l_status'],
                'f_id':row['f_id']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#List all locations for a given franchise
@app.route('/getAllLocationsActive', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllLocationsActive():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Locations WHERE f_id=? AND l_status=?", (int(request.json['id']),(request.json['status'])))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'name':row['name'],
                'address':row['address'],
                'num_screens':row['num_screens'],
                'l_status':row['l_status'],
                'f_id':row['f_id']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#Add Location
@app.route('/addNewLocation', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewLocation():
    
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        
        cur.execute("INSERT INTO Locations (name, address, num_screens, l_status, f_id) \
                    VALUES (?,?,?,?,?)", (request.json['name'],
                                        request.json['address'],
                                        int(request.json['num_screens']),
                                        request.json['l_status'],
                                        int(request.json['f_id'])))
                                                
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        result = cur.lastrowid
        cur.close()
        con.close()
    
    
    return jsonify(result)

#Update Location
@app.route('/updateLocation', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateLocation():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE Locations SET name=?, address=?, num_screens=?, l_status=? WHERE id=?",
                                                    (request.json['name'],
                                                     request.json['address'],
                                                     int(request.json['num_screens']),
                                                     request.json['l_status'],
                                                     int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete Location
@app.route('/deleteLocation', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteLocation():
    #Note that any related schedule must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from Locations WHERE id=?", (int(request.json['id']),))
        cur.execute("DELETE from Screens WHERE id=?", (int(request.json['id']),))
        cur.execute("DELETE from Movies WHERE id=?", (int(request.json['id']),))
        cur.execute("DELETE from Prices WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)





'''
    Screen Section
'''
#List all screens
@app.route('/getAllScreens', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllScreens():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Screens WHERE f_id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'number':row['number'],
                'capacity':row['capacity'],
                'l_id':row['l_id'],
                'f_id':row['f_id']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#Add Screen
@app.route('/addNewScreen', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewScreen():
    
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        
        cur.execute("INSERT INTO Screens (number, capacity, l_id, f_id) VALUES (?,?,?,?)",
                                        (int(request.json['number']),
                                         int(request.json['capacity']),
                                         int(request.json['l_id']),
                                         int(request.json['f_id'])))
                                        
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        result = cur.lastrowid
        cur.close()
        con.close()
    
    
    return jsonify(result)

#Update Screen
@app.route('/updateScreen', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateScreen():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE Screens SET number=?, capacity=? WHERE id=?",
                    (int(request.json['number']),
                     int(request.json['capacity']),
                     int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete Screen
@app.route('/deleteScreen', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteScreen():
    #Note that any related schedule must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from Screens WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)







'''
    Movie Section
'''

#List all movies
@app.route('/getAllMovies', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllMovies():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Movies WHERE f_id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'movie_img':row['movie_img'],
                'title':row['title'],
                'director':row['director'],
                'age_rating':row['age_rating'],
                'duration':row['duration'],
                'genre':row['genre'],
                'description':row['description'],
                'l_id':row['l_id'],
                'f_id':row['f_id']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#Add Movie
@app.route('/addNewMovie', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewMovie():
    
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        
        cur.execute("INSERT INTO Movies (title, director, description, movie_img, age_rating, duration, genre, l_id, f_id) \
                    VALUES (?,?,?,?,?,?,?,?,?)", (request.json['title'],
                                                request.json['director'],
                                                request.json['description'],
                                                request.json['movie_img'],
                                                request.json['age_rating'],
                                                int(request.json['duration']),
                                                request.json['genre'],
                                                int(request.json['l_id']),
                                                int(request.json['f_id'])))
                     
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        result = cur.lastrowid
        cur.close()
        con.close()
    
    return jsonify(result)

#Update Movie
@app.route('/updateMovie', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateMovie():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE Movies SET title=?, director=?, description=?, age_rating=?, duration=?, genre=?, movie_img=? WHERE id=?",
                        (request.json['title'],
                         request.json['director'],
                         request.json['description'],
                         request.json['age_rating'],
                         int(request.json['duration']),
                         request.json['genre'],
                         request.json['movie_img'],
                         int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete Movie
@app.route('/deleteMovie', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteMovie():
    #Note that any related schedule must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from Movies WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)
                    

                    
                    
                    
                    
                    
'''
    Price Section
    '''

#List all prices
@app.route('/getAllPrices', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllPrices():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Prices WHERE f_id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'priceType':row['priceType'],
                'adult':row['adult'],
                'child':row['child'],
                'oap':row['oap'],
                'student':row['student'],
                'l_id':row['l_id'],
                'f_id':row['f_id'],
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#List all prices
@app.route('/getAllPricesForLocation', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllPricesForLocation():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Prices WHERE f_id=? AND l_id=?", (int(request.json['f_id']), int(request.json['l_id'])))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'priceType':row['priceType'],
                'adult':row['adult'],
                'child':row['child'],
                'oap':row['oap'],
                'student':row['student'],
                'l_id':row['l_id'],
                'f_id':row['f_id'],
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#Add Price
@app.route('/addNewPrice', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewPrice():
    
    result = 0
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        
        cur.execute("INSERT INTO Prices (priceType, adult, child, oap, student, l_id, f_id) \
                    VALUES (?,?,?,?,?,?,?)", (request.json['priceType'],
                                              float(request.json['adult']),
                                              float(request.json['child']),
                                              float(request.json['oap']),
                                              float(request.json['student']),
                                              int(request.json['l_id']),
                                              int(request.json['f_id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        result = cur.lastrowid
        cur.close()
        con.close()
    
    return jsonify(result)

#Update Price
@app.route('/updatePrice', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updatePrice():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE Prices SET priceType=?, adult=?, child=?, oap=?, student=?,\
                     l_id=?, f_id=? WHERE id=?", (request.json['priceType'],
                                                  float(request.json['adult']),
                                                  float(request.json['child']),
                                                  float(request.json['oap']),
                                                  float(request.json['student']),
                                                  int(request.json['l_id']),
                                                  int(request.json['f_id']),
                                                  int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete Price
@app.route('/deletePrice', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deletePrice():
    #Note that any related price must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from Prices WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)




'''
    Schedule Section
    '''

#List all schedules
@app.route('/getAllMovieSchedules', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllMovieSchedules():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM MovieSchedule WHERE f_id=?", (int(request.json['id']),))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'l_id':row['l_id'],
                'f_id':row['f_id'],
                's_id':row['s_id'],
                'm_id':row['m_id'],
                'movie_time':row['movie_time'],
                'curr_status':row['curr_status']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#List all schedules
@app.route('/getLocationMovieSchedules', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getLocationMovieSchedules():
    
    list = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM MovieSchedule ms \
                    LEFT JOIN Movies m ON ms.m_id = m.id \
                    LEFT JOIN Screens s ON ms.s_id = s.id\
                    WHERE ms.f_id=? AND ms.l_id=?", (int(request.json['f_id']),int(request.json['l_id'])))
        rows = cur.fetchall()
        for row in rows:
            item = {
                'id':row['id'],
                'l_id':row['l_id'],
                'f_id':row['f_id'],
                's_id':row['s_id'],
                'number':row['number'],
                'capacity':row['capacity'],
                'm_id':row['m_id'],
                'title':row['title'],
                'director':row['director'],
                'description':row['description'],
                'movie_img':row['movie_img'],
                'age_rating':row['age_rating'],
                'duration':row['duration'],
                'genre':row['genre'],
                'movie_time':row['movie_time'],
                'tickets_booked':row['tickets_booked'],
                'curr_status':row['curr_status']
            }
            list.append(item)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(list) == 0):
        return "None"
    
    return jsonify(list)

#Add Movie Schedule
@app.route('/addNewMovieSchedule', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def addNewMovieSchedule():
    
    result = 0
    
    tempList = request.json['list']
    
    for sched in tempList:
        try:
            print(sched)
            con = sql.connect(myDB)
            cur = con.cursor()
        
        
        
            cur.execute("INSERT INTO MovieSchedule (f_id, l_id, s_id, m_id, movie_time, tickets_booked, curr_status) \
                        VALUES (?,?,?,?,?,?,?)", (int(sched['f_id']),
                                              int(sched['l_id']),
                                              int(sched['s_id']),
                                              int(sched['m_id']),
                                              sched['movie_time'],
                                              int(sched['tickets_booked']),
                                              sched['curr_status']))
            con.commit()
        except:
            result = -1
            con.rollback()
        finally:
            result = cur.lastrowid
            cur.close()
            con.close()

    
    return jsonify(result)

#Update Movie Schedule
@app.route('/updateMovieSchedule', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateMovieSchedule():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE MovieSchedule SET s_id=?, m_id=?, movie_time=?, curr_status=?,\
                    WHERE id=?", (int(request.json['s_id']),
                                  int(request.json['m_id']),
                                  request.json['movie_time'],
                                  request.json['curr_status'],
                                  int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Update Movie Schedule ticket bookings number
@app.route('/updateMovSchedTicketsSold', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def updateMovSchedTicketsSold():
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("UPDATE MovieSchedule SET tickets_booked=tickets_booked+? WHERE id=?",
                            (int(request.json['tickets_booked']),
                             int(request.json['id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)


#Delete all Movie Schedules for a screen if movie is changed
@app.route('/deleteAllMovieSchedule', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteAllMovieSchedule():
    #Note that any related price must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from MovieSchedule WHERE l_id=? AND s_id=?", (int(request.json['l_id']),int(request.json['s_id'])))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#Delete Movie Schedule
@app.route('/deleteMovieSchedule', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteMovieSchedule():
    #Note that any related price must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from MovieSchedule WHERE id=?", (int(request.json['id']),))
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

                    
                    

#Delete Movie
@app.route('/deleteSchedulesForMovie', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def deleteSchedulesForMovie():
#Note that any related schedule must also be deleted
    result = 1
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        cur.execute("DELETE from MovieSchedule WHERE s_id=?", (int(request.json['s_id']),))
                    
        con.commit()
    except:
        result = -1
        con.rollback()
    finally:
        cur.close()
        con.close()
                                
    return jsonify(result)



'''
    Booking tickets methods
    '''
@app.route('/bookTickets', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def bookTickets():
    result = ""
    try:
        con = sql.connect(myDB)
        cur = con.cursor()
        today = date.today()
        
        
        print(request.data)
        
        cur.execute("INSERT INTO Bookings (f_id, l_id, s_id, m_id, booking_Date, show_time, num_adults, num_child, num_oap,\
                     num_student, total_cost) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                        (int(request.json['f_id']),
                         int(request.json['l_id']),
                         int(request.json['s_id']),
                         int(request.json['m_id']),
                         today,
                         #"2017-04-27",
                         request.json['show_time'],
                         int(request.json['num_adults']),
                         int(request.json['num_child']),
                         int(request.json['num_oap']),
                         int(request.json['num_student']),
                         float(request.json['total_cost'])))
        con.commit()
        result = "Booking Successful"
    except:
        result = "Booking Failed"
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    return jsonify(result)

#List all schedules
@app.route('/getAllReports', methods=['POST','OPTIONS'])
@crossdomain(origin='*')
def getAllReports():
    
    bookingList = []
    try:
        con = sql.connect(myDB)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT * FROM Bookings WHERE f_id=?", (int(request.json['id']),))

        rows = cur.fetchall()
        for row in rows:
            booking = {
                'id':row['id'],
                'f_id':row['f_id'],
                'l_id':row['l_id'],
                's_id':row['s_id'],
                'm_id':row['m_id'],
                'booking_Date':row['booking_Date'],
                'show_time':row['show_time'],
                'num_adults':row['num_adults'],
                'num_child':row['num_child'],
                'num_oap':row['num_oap'],
                'num_student':row['num_student'],
                'total_cost':row['total_cost']
            }
            print(booking)
            bookingList.append(booking)
    except:
        con.rollback()
    finally:
        cur.close()
        con.close()
    
    if(len(bookingList) == 0):
        return "None"

    return jsonify(bookingList)




'''
    Test methods below
    '''






@app.route('/', methods=['GET','OPTIONS'])
@crossdomain(origin='*')
def index():
    con = sql.connect("database.db")
    con.row_factory = sql.Row

    cur = con.cursor()
    #cur.execute("select * from UserAccounts WHERE username!=?", ("tamoh1",))
    cur.execute("select * from Bookings")
    
    rows = cur.fetchall()
    
    userList = []
    
    for row in rows:
        user = {
            'id':row['id'],
                'f_id':row['f_id'],
                'l_id':row['l_id'],
                's_id':row['s_id'],
                'm_id':row['m_id'],
                'booking_Date':row['booking_Date'],
                'show_time':row['show_time'],
                'num_adults':row['num_adults'],
                'num_child':row['num_child'],
                'num_oap':row['num_oap'],
                'num_student':row['num_student'],
                'total_cost':row['total_cost']
        }
        userList.append(user)
    
    return jsonify(userList)

@app.route('/movies', methods=['GET','OPTIONS'])
@crossdomain(origin='*')
def movies():
    con = sql.connect("database.db")
    con.row_factory = sql.Row
    
    cur = con.cursor()
    #cur.execute("select * from UserAccounts WHERE username!=?", ("tamoh1",))
    cur.execute("select * from Locations")
    
    rows = cur.fetchall()
    
    userList = []
    
    for row in rows:
        user = {
            'id':row['id'],
                'name':row['name'],
                'address':row['address'],
                'num_screens':row['num_screens'],
                'l_status':row['l_status'],
                'f_id':row['f_id']
        }
        userList.append(user)
    
    return jsonify(userList)

@app.route('/screens', methods=['GET','OPTIONS'])
@crossdomain(origin='*')
def screens():
    con = sql.connect("database.db")
    con.row_factory = sql.Row
    
    cur = con.cursor()
    #cur.execute("select * from UserAccounts WHERE username!=?", ("tamoh1",))
    #cur.execute("select * from Screens")
    cur.execute("SELECT * FROM MovieSchedule \
                LEFT JOIN Movies ON MovieSchedule.m_id = Movies.id")
    
    rows = cur.fetchall()
    
    userList = []
    
    for row in rows:
        user = {
            'id':row['id'],
            'movie_time':row['movie_time']
        }
        userList.append(user)
    
    return jsonify(userList)

@app.route('/dataSetUp', methods=['GET'])
@crossdomain(origin='*')
def dataSetUp():
    
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            
            #cur.execute("INSERT INTO Franchise (name) VALUES ('Cineverse')")
            cur.execute("INSERT INTO Locations (name, address, num_screens, l_status, f_id) VALUES (?,?,?,?,?)",
                        ("Cork","Mahon Point",5,"active",1) )
                    #cur.execute("INSERT INTO Screens (number, capacity, l_id, f_id) VALUES (?,?,?,?)",
                    #   (1,200,1,1) )
                    #cur.execute("INSERT INTO Screens (number, capacity, l_id, f_id) VALUES (?,?,?,?)",
                    #   (1,200,2,1) )
            cur.execute("INSERT INTO UserAccounts (f_name, l_name, username, password, email, mobile, prof_img, \
                            userType, defaultLocation, f_id) VALUES (?,?,?,?,?,?,?,?,?,?)",
                            ("Thomas","O Halloran","tamoh1","123","tamoh1@gmail.com","0863778624","","super",1,1) )
                        
            con.commit()
            msg = "Set Up Completed!!!"
    except:
        con.rollback()
        msg = "Insert Error"
                                            
    finally:
        con.close()

    return msg;

@app.route('/bookingsSetUp', methods=['GET'])
@crossdomain(origin='*')
def bookingsSetUp():
    
    try:
        with sql.connect("database.db") as con:
            cur = con.cursor()
            
            cur.execute("INSERT INTO Bookings (f_id, l_id, s_id, m_id, booking_Date, show_time, num_adults, num_child, num_oap,\
                        num_student, total_cost) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                        (1, 1, 1))
                
                        
            con.commit()
            msg = "Bookings Set Up Completed!!!"
    except:
        con.rollback()
        msg = "Insert Error"
    
    finally:
        con.close()

    return msg;


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3458, debug=True)
