drop table if exists Franchise;
    create table Franchise (
        id integer primary key autoincrement,
        name text not null
    );

drop table if exists UserAccounts;
    create table UserAccounts (
        id integer primary key autoincrement,
        f_name text not null,
        l_name text not null,
        username text not null,
        password text not null,
        email text not null,
        mobile text,
        prof_img text,
        userType text not null,
        defaultLocation integer not null,
        f_id integer not null
    );
    
drop table if exists Locations;
    create table Locations (
        id integer primary key autoincrement,
        name text not null,
        address text not null,
        num_screens integer not null,
        l_status text not null,
        f_id integer not null
    );

drop table if exists Screens;
    create table Screens (
        id integer primary key autoincrement,
        number integer not null,
        capacity integer not null,
        l_id integer not null,
        f_id integer not null
    );
    
drop table if exists Movies;
    create table Movies (
        id integer primary key autoincrement,
        l_id integer not null,
        title text not null,
        director text noy null,
        description text,
        movie_img text,
        age_rating text not null,
        duration integer not null,
        genre text,
        f_id integer not null
    );
    
drop table if exists Prices;
    create table Prices (
        id integer primary key autoincrement,
        priceType text not null,
        adult double not null,
        child double not null,
        oap double not null,
        student double not null,
        l_id integer not null,
        f_id integer not null
    );
    
drop table if exists MovieSchedule;
    create table MovieSchedule (
        id integer primary key autoincrement,
        f_id integer not null,
        l_id integer not null,
        s_id integer not null,
        m_id integer not null,
        movie_time text not null,
        tickets_booked integer not null,
        curr_status text not null
    );
    
drop table if exists Bookings;
    create table Bookings (
        id integer primary key autoincrement,
        f_id integer not null,
        l_id integer not null,  
        s_id integer not null,
        m_id integer not null,
        booking_Date date not null,
        show_time string not null,
        num_adults integer,
        num_child integer,
        num_oap integer,
        num_student integer,
        total_cost double not null
    );